import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image,
} from "react-native";

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
    };
  }
  changeTextInput = (text) => {
    this.setState({ email: text });
  };
  render() {
    return (
      <View style={styles.container}>
        <Image source={require("./img/logo.png")} style={styles.logo}/>
        <Text style={styles.titulo}>Bienvenido</Text>
        <Text style={styles.subtitulo}>Inicie Sesión</Text>
        <TextInput
          style={styles.input1}
          placeholder="ejemplo1234@gmail.com"
          onChangeText={(text) => this.changeTextInput(text)}
          value={this.state.email}
        />
        <TextInput 
          style={styles.input2}
          placeholder="password"
        />
        <TouchableOpacity style={styles.button} onPress={this.onPress}>
          <Text>Ingresar</Text>
        </TouchableOpacity>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    paddingHorizontal: 10,
    backgroundColor: "darkturquoise",
  },
  logo:{
    width: 250,
    height: 100,
    margin: 65,
  },
  titulo: {
    fontSize: 50,
    color: "white",
  },
  subtitulo: {
    fontSize: 20,
    color: "white",
  },
  input1: {
    width: "80%",
    color: "black",
    height: 50,
    padding: 10,
    marginTop: 20,
    borderRadius: 30,
    borderWidth: 1,
    borderColor: "white",
  },
  input2: {
    width: "80%",
    height: 50,
    color: "black",
    padding: 10,
    marginTop: 20,
    borderRadius: 30,
    borderWidth: 1,
    borderColor: "white",
  },
  button: {
    width: 200,
    alignItems: "center",
    backgroundColor: "darkgray",
    marginTop: 30,
    padding: 10,
    borderRadius: 30,
  },
});
